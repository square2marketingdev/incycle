Bootstrap 4 new template README

Files needed to be created:

## .gitignore begin ##
```
#!text
.sass-cache
.sass-cache/*
node_modules
node_modules/*
output
output/*

```
## .gitignore end ##


Once you have the repo cloned on your local machine, you will need to add the gulp dependencies to your folder.


you will need to vist your folder where teh repo is and add this:

```
#!bash

npm init -y
npm install --save-dev gulp gulp-sass gulp-rename gulp-sourcemaps sass-include-paths gulp-run gulp-plumber gulp-notify username gulp-util vinyl-ftp gulp-debug  
```


Items you need to change in gulpfile.js :


## gulpfile.js start ##
```
#!javascript
var syncWithHubspot = true;
var ftpFolder = '/portals/2659858-test-portal-www_square2marketing_com/content/templates/custom';

```
## gulpfile.js end ##